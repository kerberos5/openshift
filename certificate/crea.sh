#!/bin/bash
# Genera una chiave privata per la tua CA custom
#openssl genpkey -algorithm RSA -out ca.key
openssl genrsa -out ca.key 4096

# Genera una richiesta di firma del certificato (CSR) per la tua CA custom
openssl req -new -key ca.key -out ca.csr -subj "/CN=RedHatInternalTrust/OU=RHT/O=RedHat/L=Roma/ST=Lazio/C=IT"

# Genera il certificato self-signed per la tua CA custom:
openssl x509 -req -signkey ca.key -in ca.csr -out ca.crt -days 365

# Genera una chiave privata per il certificato wildcard:
#openssl genpkey -algorithm RSA -out api-wildcard.key
openssl genrsa -out api-wildcard.key 4096

# Genera una richiesta di firma del certificato (CSR) per il certificato wildcard 
# utilizzando il file openssl.cnf che hai fornito:
openssl req -new -key api-wildcard.key -out api-wildcard.csr -config openssl.cnf

# Firma il certificato wildcard utilizzando la tua CA custom
openssl x509 -req -in api-wildcard.csr -out api-wildcard.crt -CA ca.crt -CAkey ca.key -CAcreateserial -extfile openssl.cnf -extensions v3_req -days 365

