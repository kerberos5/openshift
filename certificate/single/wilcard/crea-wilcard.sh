#!/bin/bash
# Genera una chiave privata per il certificato wildcard:
#openssl genpkey -algorithm RSA -out api-wildcard.key
openssl genrsa -out wildcard.key 4096

# Genera una richiesta di firma del certificato (CSR) per il certificato wildcard 
# utilizzando il file openssl.cnf che hai fornito:
openssl req -new -key wildcard.key -out wildcard.csr -config openssl.cnf

# Firma il certificato wildcard utilizzando la tua CA custom
openssl x509 -req -in wildcard.csr -out wildcard.crt -CA ../ca.crt -CAkey ../ca.key -CAcreateserial -extfile openssl.cnf -extensions v3_req -days 365
