#!/bin/bash
# Genera una chiave privata per la tua CA custom
#openssl genpkey -algorithm RSA -out ca.key
openssl genrsa -out ca.key 4096

# Genera una richiesta di firma del certificato (CSR) per la tua CA custom
openssl req -new -key ca.key -out ca.csr -subj "/CN=RedHatInternalTrust/OU=RHT/O=RedHat/L=Roma/ST=Lazio/C=IT"

# Genera il certificato self-signed per la tua CA custom:
openssl x509 -req -signkey ca.key -in ca.csr -out ca.crt -days 365
